# Ajera API Library - Laravel Wrapper

Contact: matt@mattlibera.com

# Introduction

This package is a Laravel wrapper for the Ajera API PHP Library package, so that the Ajera API PHP Library can be used in Laravel apps. 

> This is a **work in progress**. Not recommend for production apps just yet.

---

# Installation

1. `composer require 'mattlibera/ajera-api-wrapper-laravel'`
2. **IF RUNNING LARAVEL 5.5+, you may skip this step, as automatic package discovery will pick this up.** For Laravel 5.4 and below, add `MattLibera\AjeraApiLaravel\ServiceProvider::class,` to your `config/app.php` file manually.
3. Run `php artisan vendor:publish --provider='MattLibera\AjeraApiLaravel\ServiceProvider` - to publish the `ajera-api.php` config file
4. Set your environment credentials in your `.env` file, and set your configuration options in `config/ajera-api.php`

> Note: do not use protocols (http:// or https://) for `AJERA_API_HOST`. Instead put your protocol (preferably https), letters only, into `AJERA_API_PROTOCOL`

```
AJERA_API_PROTOCOL=
AJERA_API_HOST=
AJERA_API_ID=

AJERA_API_USERNAME=
AJERA_API_PASSWORD=
```

## Dependencies

This package has dependencies on `mattlibera/ajera-api-php-library`, `standaniels/flash`, and `barryvdh/laravel-debugbar` (dev)

---

# Usage

## Basic Usage / Getting Started

In your code, assuming you've set your information/credentials properly in your `.env` file, you should be able to instantiate the `MattLibera\AjeraApiLaravel\AjeraApi` class, and then use any of its available methods (inherited from `MattLibera\AjeraApi\AjeraApi`) to make an API call.

## Configuration options

### Session Key

If you wish to override the default session key that is used to store the token, then add an entry to your `.env` file:

```
AJERA_API_TOKEN_SESSION_KEY=my.session.key
```

This is where the session token will be stored and looked up by the wrapper so that we don't have to make excess calls to the `StartAPISession` endpoint.

To set the timeframe within which the session token is valid, you can use `AJERA_API_TOKEN_VALID_SECONDS` in your `.env` file to override (default is 1 hour). With this in place, a new session will be started after that many seconds regardless of what the app's session contains, to ensure that an expired token is not sent to the Ajera API.

### Notification Mode

There are two notification options built into this library. You may flash the information on the screen as a flash message, or log it in the Laravel logs (whatever you've got set).

In the `config/ajera-api.php` file, `notification_mode` is an array. Inside, add one or both of `flash` and `log`.
 
If you choose to roll this into a higher-level custom notification / logging system for your app, you can leave this array empty to disable both of these methods, and then handle your own logging based on the return array from the API call (the API class returns the entire response, including response code, from every call).

### Debug Mode

_Requires DebugBar by Barryvdh (`barryvdh\laravel-debugbar`)_

If you turn on Debug Mode in the `config/ajera-api.php` file, extra information about the call will be written to the Debugbar. This includes timing, hashed cache key, and full results from each call.
 
 > Note: in accordance with best practices for this package, this will only work if the app environment is not set to `production`
 
## Caching
 
For speed, adherence to rate limits, and overall performance, API call caching is built into this library. This utilizes the Laravel Cache mechanism (which, like Logs, can be set however you want in the Laravel config). You can optionally disable this by adding `AJERA_API_CACHING=off` in your `.env` file.

This caching isn't fancy; it'll literally just cache the result body from the API call. Depending on what your app is going to do, you may want to take this a step further and cache into some database tables. But this is here for you, for whatever that's worth to you.

### Cache TTL

The TTL of the cache is set to 10 minutes by default, but by adding `AJERA_API_CACHE_MINUTES=x` to your `.env` file, you can set the cache to expire after x minutes.

### Cached endpoints

Caching is performed only for specific endpoints (e.g. requests that would equate to typical `GET` requests). Obviously, you would not want to cache requests that are designed to actually modify information in Ajera (equivalent to `POST` or `PATCH` or `DELETE` requests). 

By default, there is a basic set of endpoints set up in `config/ajera-api.php`. If you would like change which endpoints are / are not cached (or add your own), simply modify the `cached_endpoints` array in the config file.

In Ajera, all endpoints are hit via POST request, and so you'll want to specify the "Request" name in `cached_endpoints` - for instance "GetContacts".

---

# Version History

## 0.1

- First real release. 
- `.env` integration for Ajera credentials
- Debug mode
- Caching