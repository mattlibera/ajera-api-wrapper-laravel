<?php

namespace MattLibera\AjeraApiLaravel;

use Illuminate\Support\Facades\Cache;
use Barryvdh\Debugbar\Facade as Debugbar;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class AjeraApi extends \MattLibera\AjeraApi\AjeraApi {

    protected $historyMonths;

    protected $cacheActive;
    protected $endpointsToCache;
    protected $cacheMinutes;

    protected $debugMode;

    protected $tokenSessionKey;
    protected $createdSessionKey;
    protected $tokenValidSeconds;

    public function __construct() {

        // set vars specific to this instance of Laravel
        $this->setId(config('ajera-api.api_id'));
        $this->setHost(config('ajera-api.api_host'));
        $this->setProtocol(config('ajera-api.protocol'));
        $this->setUsername(config('ajera-api.username'));
        $this->setPassword(config('ajera-api.password'));

        if (env('HTTP_REQUEST_USE_PROXY') == 'true') {
            $this->setUseProxy(true);
            $this->setProxyHost(env('HTTP_REQUEST_PROXY_HOST'));
            $this->setProxyPort(env('HTTP_REQUEST_PROXY_PORT'));
        }

        // cache configuration
        $this->cacheActive = config('ajera-api.cache_active');
        $this->endpointsToCache = config('ajera-api.cache_endpoints');
        $this->cacheMinutes = config('ajera-api.cache_minutes');

        // debug mode (not enabled in production)
        $this->debugMode = (config('app.env') == 'production') ? false : config('ajera-api.debug_mode');

        // session key info
        $this->tokenSessionKey = config('ajera-api.session_key');
        $this->createdSessionKey = $this->tokenSessionKey . '_created';
        $this->tokenValidSeconds = config('ajera-api.session_key_valid_seconds');
    }

    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    */


    /**
     * checkForSessionToken()
     *
     * gets session token if it is not already set. calls startSession() which by default will fetch new
     * from the Ajera API. user app can override this method in a child class to define the way in which
     * the app fetches the session (e.g. if the app stores the token in the browser session, or the
     * database, the app code can extend this class and redefine checkForSessionToken() to do what it
     * needs).
     *
     * @throws \Exception
     */
    protected function checkForSessionToken() {
        if (empty($this->sessionToken)) {

            try {
                $sessionValue = session($this->tokenSessionKey);
                $createdValue = session($this->createdSessionKey);
                if (empty($sessionValue)) {
                    throw new \Exception('Ajera API session token is not in app session; getting new');
                }

                if (is_null($createdValue)) {
                    throw new \Exception('Token does not have valid expiration date; getting new.');
                }

                $created = Carbon::parse($createdValue);
                if ($created->addSeconds($this->tokenValidSeconds)->lte(Carbon::now())) {
                    throw new \Exception('Token exists in app session but is expired.');
                }

                $this->setSessionToken(session($this->tokenSessionKey));
                Log::info('retrieved Ajera API session token from app session');

            } catch (\Exception $e) {
                Log::info($e->getMessage());
                $this->getNewSessionToken();
            }

        } else {
            Log::info('Ajera API session token already exists on the object.');
        }
    }

    public function getNewSessionToken() {

        try {
            $token = $this->startSession();
            Log::info('Retrieved new session token from Ajera API.');

            $createdTime = Carbon::now()->toDateTimeString();

            session([
                $this->tokenSessionKey => $token,
                $this->createdSessionKey => $createdTime
            ]);
            $this->setSessionToken($token);
            Log::info('Stored new session token from Ajera API in app session. Token created at ' . $createdTime);
        } catch (\Exception $e) {
            Log::warning('Error while retrieving and storing session token: ' . $e->getMessage());
            throw $e;
        }
    }

    /**
     * apiCall() - performs a call to the Ajera API
     *
     * Notes: all API calls are the POST method. All data passed in is in JSON format.
     *
     * @param string $method - command type (CreateAPISession, ListClients, etc.)
     * @param array $methodArguments - array that will be sent as JSON to the API endpoint.
     * @param array $requestOptions - Guzzle options for the request.
     *
     * @return array
     */

    protected function apiCall($method, $methodArguments = [], $requestOptions = []) {

        $hashedCacheKey = hash('sha256', $method . json_encode($methodArguments) . json_encode($requestOptions));
        $this->debugMessage('Hashed cache key: ' . $hashedCacheKey);

        if ($this->cacheActive && Cache::has($hashedCacheKey)) {
            // in cache
            $resultArray = Cache::get($hashedCacheKey);
            $returnArray = [
                'source' => 'cache',
                'created' => Cache::get($hashedCacheKey . '-created', 'unknown')
            ];
            $message = 'API data for \'' . $method . '\' retrieved from cache (created: ' . $returnArray['created'] . ')';
            $messageLevel = 'success';
        } else {
            // not in cache or cache inactive
            $this->debugStartMeasure('apiCall', 'Making API call to Ajera');
            $resultArray = parent::apiCall($method, $methodArguments, $requestOptions);
            $this->debugStopMeasure('apiCall');

            $created = time();

            // cache if the call was successful, and we are supposed to cache it...
            if ($resultArray['response']['httpCode'] == 200) {
                $messageLevel = 'success';
                $message = 'API call to \'' . $method . '\' successful.';

                if ($this->cacheActive && strtolower($method) == 'get') {
                    if (in_array($method, $this->endpointsToCache)) {
                        Cache::put($hashedCacheKey, $resultArray, $this->cacheMinutes);
                        Cache::put($hashedCacheKey . '-created', $created, $this->cacheMinutes);
                        $message .= ' Result cached in API cache.';
                    } else {
                        $message .= ' Not a cacheable API call.';
                    }
                }
            } else {
                $message = 'API error calling \'' . $method . '\': ' . $resultArray['response']['httpCode'] . ' - ' . $resultArray['response']['httpReason'] . ' (' . $resultArray['response']['message'] . ')';
                $messageLevel = 'danger';
            }
            $returnArray = [
                'source' => 'api',
                'created' => $created
            ];
        }

        // handle notifications / logging / etc.
        $notificationMode = config('ajera-api.notification_mode');

        if (in_array('flash', $notificationMode)) {
            flash($message, $messageLevel);
        }

        if (in_array('log', $notificationMode)) {
            if ($messageLevel == 'danger') {
                Log::error($message);
            } else {
                Log::info($message);
            }
        }

        $finalArray = array_merge($returnArray, $resultArray);

        $this->debugInfo($finalArray);

        return array_merge($finalArray);
    }

    // debug helpers

    protected function debugMessage($message, $label = null) {
        if ($this->debugMode) {
            if (!is_null($label)) {
                Debugbar::addMessage($message, $label);
            } else {
                Debugbar::addMessage($message);
            }
        }
    }

    protected function debugInfo($object) {
        if ($this->debugMode) {
            Debugbar::info($object);
        }
    }

    protected function debugStartMeasure($key, $label = '') {
        if ($this->debugMode) {
            Debugbar::startMeasure($key, $label);
        }
    }

    protected function debugStopMeasure($key) {
        if ($this->debugMode) {
            Debugbar::stopMeasure($key);
        }
    }


    /*
    |--------------------------------------------------------------------------
    | Custom
    |--------------------------------------------------------------------------
    |
    | Utilizing the API to do things that it can't normally do
    |
    */

    public function listProjectsForClient($id, $methodArguments = []) {
        $body = [
            'MethodArguments' => $methodArguments
        ];

        $result = $this->apiCall('ListProjects', $body);

        dd($result);

        if ($result['response']['success']) {
            return $this->returnRaw ? $result : $result['body']['Content']['Projects'];
        } else {
            return [];
        }
    }
}