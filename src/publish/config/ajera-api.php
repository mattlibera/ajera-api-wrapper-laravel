<?php

return [

    // URLs

    'api_host' => env('AJERA_API_HOST'),
    'api_id' => env('AJERA_API_ID'),
    'protocol' => env('AJERA_API_PROTOCOL', 'https'), // default https

    // CREDENTIALS

    'username' => env('AJERA_API_USERNAME'),
    'password' => env('AJERA_API_PASSWORD'),

    // SETTINGS

    'session_key' => env('AJERA_API_TOKEN_SESSION_KEY', 'ajeraapi.sessiontoken'),
    'session_key_valid_seconds' => env('AJERA_API_TOKEN_VALID_SECONDS', 3600), // 1 hour default

    'notification_mode' => [], // flash, log, or empty array for none

    'debug_mode' => false, // true - extra logging in Barryvdh Debugbar (required)

    // CACHE

    'cache_active' => env('AJERA_API_CACHING', 'on') == 'on', // set to 'on' or 'off' in .env file
    'cache_minutes' => env('AJERA_API_CACHE_MINUTES', 10),
    'cache_endpoints' => [
        'ListClients',
        'GetClients',
        'ListEmployees',
        'GetEmployees',
        'ListContacts',
        'GetContacts',
        'ListVendors',
        'GetVendors',
        'ListProjects',
        'GetProjects',
        'ListExpenseReports',
        'GetExpenseReports',
        'ListActivities',
        'ListBankAccounts',
        'ListChargeablePhases',
        'ListCompanies',
        'ListEmployeeCreditCards',
        'ListInvoiceFormats',
    ],

];